#include "Logging.h"
#include "Game.h"

int main() {

	Logger::Init();

	Game* game = new Game();
	game->Run();
	delete game;

	Logger::Uninitialize();

	return 0;
}